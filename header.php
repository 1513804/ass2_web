<?php
  $current_page = basename($_SERVER['PHP_SELF'], ".php");
  $title_page = "";
  switch ($current_page) {
    case "history":
      $title_page = "Lịch sử";
      break;
    case "ourculture":
      $title_page = "Văn hóa doanh nghiệp";
      break;
    case "product":
      $title_page = "Sản phẩm";
      break;
    case "contact":
      $title_page = "Liên hệ";
      break;
    case "recruitment":
      $title_page = "Tuyển dụng";
      break;
    default:
      $title_page = "Công ty VNG";
  }
?>

<!DOCTYPE html>
<html lang="vi">
<head>
  <title><?php echo $title_page; ?></title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Charm&family=Texturina:wght@200&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
  <div class="nav_header bg-while">
    <div class="container h-100">
      <div class="logo h-100 d-flex align-items-center justify-content-between">
        <a href="HomePage.php">
          <img src="./images/vng-logo.png" alt="logovng" />
        </a>
        <nav class="nav_mobile navbar navbar-expand-lg">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fal fa-bars"></i>
          </button>
  
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link" href="HomePage.php">trang chủ</a>
              </li>
              <li class="nav-item dropdown <?php if($current_page == "history" || $current_page == "ourculture") echo "active"; ?>">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  chúng tôi
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="history.php">lịch sử</a>
                  <a class="dropdown-item" href="ourculture.php">văn hoá doanh nghiệp</a>
                </div>
              </li>
              <li class="nav-item <?php if($current_page == "product") echo "active"; ?>">
                <a class="nav-link" href="product.php">sản phẩm</a>
              </li>
              <li class="nav-item dropdown <?php if($current_page == "contact" || $current_page == "recruitment") echo "active"; ?>">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">liên hệ</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                  <a class="dropdown-item" href="./contact.php">văn phòng</a>
                  <a class="dropdown-item" href="recruitment.php">tuyển dụng</a>
                </div>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </div>
