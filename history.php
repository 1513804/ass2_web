<?php include "header.php"; ?>

<div class="container history text-center">
  <div class="row" style="min-height: 450px;">
    <div class="col-12">
      <h2>Hành trình phát triển của VNG</h2>
      <p>Thành lập từ năm 2004 đến nay, VNG đã trải qua 14 năm phát triển và mở rộng để trở thành một trong những công ty Internet & Công nghệ hàng đầu Việt Nam và Đông Nam Á, với hơn 3200 thành viên.</p>
      <div class="timeline">
        <hr>
        <ul class="d-flex">
          <li><img src="images/1.png" alt=""><br><i class="fas fa-circle"></i><br>KHỞI NGHIỆP<br><span>2004-2005</span></li>
          <li><img src="images/2.png" alt=""><br><i class="fas fa-circle"></i><br>BỨC PHÁ<br><span>2006-2008</span></li>
          <li><img src="images/3.png" alt=""><br><i class="fas fa-circle"></i><br>ĐỊNH VỊ<br><span>2009-2012</span></li>
          <li><img src="images/4.png" alt=""><br><i class="fas fa-circle"></i><br>NÂNG TẦM<br><span>2013-2016</span></li>
          <li><img src="images/5.png" alt=""><br><i class="fas fa-circle"></i><br>HỘI NHẬP<br><span>2017-now</span></li>
        </ul>
      </div>
      
      <div class="content">
        <p class="d-none">Mở đường cho kỷ nguyên game nhập vai tại Việt Nam với thành công kỷ lục của Võ Lâm Truyền Kỳ: 20.000 PCU (lượng người chơi truy cập tại cùng một thời điểm).</p>
        <p class="d-none">Doanh thu năm 2006 đạt 17 triệu USD, gấp 6 lần năm 2005. Quy mô nhân sự tăng lên hơn 1000 người. Năm 2007, khánh thành Trung tâm dữ liệu hiện đại nhất Việt Nam, làm chủ việc lưu trữ thông tin toàn bộ sản phẩm đang cung cấp.</p>
        <p class="d-none">Ra mắt mạng xã hội đầu tiên của Việt Nam: Zing Me. Sản xuất thành công game thuần Việt MMO đầu tiên của Đông Nam Á: Thuận Thiên Kiếm. Doanh nghiệp Việt đầu tiên phát hành game ở nước ngoài khi xuất khẩu thành công game trực tuyến thuần Việt "Ủn ỉn" sang Nhật Bản.</p>
        <p class="d-none">Được World Startup Report định giá 1 tỷ USD, trở thành Startup kỳ lân duy nhất của Việt Nam. Được vinh danh “Doanh nghiệp phát triển nhanh toàn cầu tại khu vực Đông Á” tại Diễn đàn Kinh tế Thế giới 2015 (Manila, Philippines).</p>
        <p class="d-none">Top 6 thương hiệu DN CNTT hàng đầu Việt Nam, năm 2019. VNG nhận giải thưởng “Nơi làm việc tốt nhất Châu Á” do HR Asia, tạp chí uy tín hàng đầu về Nhân sự tại Châu Á tổ chức và bình chọn.</p>
      </div>
    </div>
  </div>
</div>

<?php include "footer.php"; ?>