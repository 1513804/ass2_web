<?php include "header.php"; ?>

<section class="container-fluid ourculture text-center">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 offset-lg-3">
        <h2>Sứ mệnh</h2>
        <h3 class="quote"><q>Kiến tạo công nghệ và Phát triển con người. Vì một cuộc sống tốt đẹp hơn</q></h3>
        <p>VNG tin vào sức mạnh thay đổi cuộc sống của Internet và không ngừng phát triển để mang đến cho người dùng những trải nghiệm ý nghĩa.</p>
      </div>
    </div>
  </div>
</section>
<section class="container-fluid ourculture text-center bg-while">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Giá trị cốt lỗi</h2>
        <p>Thế mạnh của VNG chính là con người và văn hóa doanh nghiệp. Dựa vào kim chỉ nam là 3 giá trị cốt lõi, các thành viên ở VNG luôn có tinh thần cống hiến vì sự phát triển chung của VNG và cộng đồng.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-4">
        <img class="img-responsive" src="images/don_nhan_thach_thuc.png" alt="">
        <p>Đón Nhận Thách Thức</p>
      </div>
      <div class="col-4">
        <img class="img-responsive" src="images/phat_trien_doi_tac.png" alt="">
        <p>Phát Triển Đối Tác</p>
      </div>
      <div class="col-4">
        <img  class="img-responsive" src="images/giu_gin_chinh_truc.png" alt="">
        <p>Giữ Gìn Chính Trực</p>
      </div>
    </div>
  </div>
</section>
<section class="container-fluid text-center">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Logo</h2>
        <p>Với hình dáng cách điệu một ngọn lửa đang cháy, logo VNG biểu trưng cho 3 giá trị quan trọng của công ty: nhiệt huyết, cầu tiến và năng động.</p>
        <p>Hình dáng vươn lên thể hiện sức trẻ và tinh thần dấn thân.</p>
        <p>Gồm 3 màu cơ bản của ngọn lửa: da cam, xanh lam và vàng</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 logo">
        <img class="img-responsive" src="images/logo1.png" alt="">
        <p>Màu cam tượng trưng cho người VNG nhiệt huyết, đoàn kết và tài năng</p>
      </div>
      <div class="col-lg-4 logo">
        <img class="img-responsive" src="images/logo2.png" alt="">
        <p>Màu xanh đặc trưng của công ty hoạt động trong lĩnh vực Internet và công nghệ.</p>
      </div>
      <div class="col-lg-4 logo">
        <img  class="img-responsive" src="images/logo3.png" alt="">
        <p>Màu vàng thể hiện thông điệp luôn mang lại giá trị tốt nhất cho khách hàng.</p>
      </div>
    </div>
  </div>
</section>
<section class="container-fluid ourculture text-center bg-while">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 offset-lg-3">
        <h2>Slogan</h2>
        <h3 class="quote"><q>Đón nhận thách thức</q></h3>
        <p>Thể hiện khát vọng chinh phục, đổi mới và vươn lên của tập thể VNG.</p>
      </div>
    </div>
  </div>
</section>

<?php include "footer.php"; ?>