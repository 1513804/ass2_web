<?php include "header.php"; ?>
  
<div class="container-fluid mt-5">
  <div class="container product">
    <div class="row intro text-center">
      <div class="col-12">
        <h2>Sản Phẩm</h2>
        <p>Hệ sinh thái ứng dụng và dịch vụ của VNG bao gồm 4 nhóm chính, tập trung hướng đến những trải nghiệm phong phú và giải pháp tối ưu, phục vụ cho người dùng cá nhân và tổ chức. Bên cạnh đó, VNG còn nghiên cứu và phát triển nhiều dự án công nghệ mới nhằm hỗ trợ hiệu quả cho hoạt động của các cơ quan quản lý và doanh nghiệp.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4 col-12">
        <div class="title d-flex">
          <img src="images/03.svg" width="70" height="70" alt="">
          <h3>Trò chơi trực tuyến</h3>
        </div>
        <ul>
          <li>Phát triển và sản xuất trò chơi trực tuyến, phát hành ra thị trường quốc tế. Sản phẩm: Khu vườn trên mây, Dead Target, iCá…).</li>
          <li>Nhập khẩu và Phát hành các trò chơi nổi tiếng thế giới. Sản phẩm: Võ Lâm Truyền Kỳ, Rules of Survival, Crossfire Legend…</li>
        </ul>
      </div>
      <div class="col-sm-8 col-12">
        <div class="row list-game text-center">
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/VLTK.png" alt="Võ lâm truyền kỳ">
            <h4><a href="#">Võ lâm truyền kỳ</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/JXM.png" alt="Võ lâm truyền kỳ Mobile">
            <h4><a href="#">Võ lâm truyền kỳ Mobile</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/KiemThe.png" alt="Kiếm thế">
            <h4><a href="#">Kiếm thế</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/Logo_KVTM.png" alt="Khu vườn trên mây">
            <h4><a href="#">Khu vườn trên mây</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/Gunny.png" alt="Gunny">
            <h4><a href="#">Gunny</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/NSTT_TV.png" alt="Ngôi sao thời trang">
            <h4><a href="#">Ngôi sao thời trang</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/CF.png" alt="CROSSFIRE LEGENDS">
            <h4><a href="#">CROSSFIRE LEGENDS</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/CuuAm3D.png" alt="Cửu âm 3D">
            <h4><a href="#">Cửu âm 3D</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/logo_360game.png" alt="360 Games">
            <h4><a href="#">360 Games</a></h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<hr>
<div class="container-fluid mb-5">
  <div class="container product">
    <div class="row">
      <div class="col-sm-4 col-12">
        <div class="title d-flex">
          <img src="images/01.svg" width="70" height="70" alt="">
          <h3>Nền tảng kết nối</h3>
        </div>
        <ul>
          <li>Các nền tảng kết nối đa dạng, đa dịch vụ, phục vụ toàn diện nhu cầu kết nối, giải trí, tìm kiếm của cá nhân và tổ chức.</li>
          <li>Nền tảng OTT Zalo, hệ sinh thái mạng xã hội giải trí Zing (Zing TV, Zing MP3…)</li>
          <li>Các nền tảng vCS, 123Go, 123Phim, công cụ tìm kiếm Laban.vn….</li>
        </ul>
      </div>
      <div class="col-sm-8 col-12">
        <div class="row list-game text-center">
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/logo_CSM.png" alt="CSM">
            <h4><a href="#">CSM</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/logo_360live.png" alt="360live">
            <h4><a href="#">360live</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/123CS_1.png" alt="vCS">
            <h4><a href="#">vCS</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/123Go.png" alt="123GO">
            <h4><a href="#">123GO</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/123phim.png" alt="123PHIM">
            <h4><a href="#">123PHIM</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/Bao_moi.png" alt="Báo mới">
            <h4><a href="#">Báo mới</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/la_ban_key.png" alt="La bàn">
            <h4><a href="#">La bàn</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/zalo.png" alt="Zalo">
            <h4><a href="#">Zalo</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/Zing_mp3.png" alt="Zing Mp3">
            <h4><a href="#">Zing MP3</a></h4>
          </div>
          <div class="col-4 bg-while item-game">
            <img class="img-fluid" src="images/Zing_movie.png" alt="Zing TV">
            <h4><a href="#">Zing TV</a></h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include "footer.php"; ?>