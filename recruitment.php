<?php include "header.php"; ?>

<div class="container-fluid">
  <div class="container recruitment bg-while">
    <div class="row">
      <div class="col-md-3">
        <h2>Các ngành nghề</h2>
        <ul>
          <li class="career active"><a href="#">Kỹ thuật / Lập trình</a></li>
          <li class="career"><a href="#">Kinh doanh / Tiếp thị</a></li>
          <li class="career"><a href="#">Thiết kế / Sáng tạo</a></li>
          <li class="career"><a href="#">Hỗ trợ kinh doanh</a></li>
        </ul>
      </div>
      <div class="col-md-5">
        <h2>Vị trí đang tuyển</h2>
        <div class="position">
          <p class="date text-primary">Hạn chót: 27/12/2020</p>
          <p class="title" title="Bấm để xem thêm thông tin"><a href="#" onclick="displayDescribe(1)">Software Engineer (Java)</a></p>
          <p class="address">TP. Hồ Chí Minh</p>
          <div class="describe d-none">
            <h3>Mô tả công việc</h3>
            <ul>
              <li>Tham gia vận hành và phát triển hệ thống ZingID và Game Payment</li>
              <li>Nghiên cứu và ứng dụng vào hệ thống nhằm đảm bảo độ tin cậy và khả năng mở rộng</li>
              <li>Phát triển các công vụ đáp ứng các công việc vận hành</li>
            </ul>
            <h3>Yêu cầu</h3>
            <ul>
              <li>Có kinh nghiệm Java Core từ 2 năm trở lên (open level tới Senior)</li>
              <li>Có kinh nghiệm CSDL RDBMS, NoSQL</li>
              <li>Có kinh nghiệm sử dụng caching</li>
              <li>Nắm vững cấu trúc dữ liệu và thuật toán cơ bản</li>
              <li>Hiểu về kiến trúc Microservices, HTTP protocol, REST/SOAP webservice</li>
              <li>Có kiến thức cơ bản với Linux</li>
              <li>Có kinh nghiệm sử dụng Messaging System: kaffa, ZeroMQ, Rabbit MQ là 1 lợi thế</li>
              <li>Tư duy tích cực, có kỹ năng giải quyết vấn đề, phối hợp tốt với các thành viên khác, nhiệt tình tham gia các hoạt động xây dựng team</li>
            </ul>
            <div class="text-center">
              <button type="submit" class="btn btn-primary btn-sm">Ứng tuyển</button>
            </div>
          </div>
          <hr>
        </div>

        <div class="position">
          <p class="date text-primary">Hạn chót: 27/12/2020</p>
          <p class="title" title="Bấm để xem thêm thông tin"><a href="#" onclick="displayDescribe(2)">QC Engineer</a></p>
          <p class="address">TP. Hồ Chí Minh</p>
          <div class="describe d-none">
            <h3>Mô tả công việc</h3>
            <ul>
              <li>Tham gia vận hành và phát triển hệ thống ZingID và Game Payment</li>
              <li>Nghiên cứu và ứng dụng vào hệ thống nhằm đảm bảo độ tin cậy và khả năng mở rộng</li>
              <li>Phát triển các công vụ đáp ứng các công việc vận hành</li>
            </ul>
            <h3>Yêu cầu</h3>
            <ul>
              <li>Có kinh nghiệm Java Core từ 2 năm trở lên (open level tới Senior)</li>
              <li>Có kinh nghiệm CSDL RDBMS, NoSQL</li>
              <li>Có kinh nghiệm sử dụng caching</li>
              <li>Nắm vững cấu trúc dữ liệu và thuật toán cơ bản</li>
              <li>Hiểu về kiến trúc Microservices, HTTP protocol, REST/SOAP webservice</li>
              <li>Có kiến thức cơ bản với Linux</li>
              <li>Có kinh nghiệm sử dụng Messaging System: kaffa, ZeroMQ, Rabbit MQ là 1 lợi thế</li>
              <li>Tư duy tích cực, có kỹ năng giải quyết vấn đề, phối hợp tốt với các thành viên khác, nhiệt tình tham gia các hoạt động xây dựng team</li>
            </ul>
            <div class="text-center">
              <button type="submit" class="btn btn-primary btn-sm">Ứng tuyển</button>
            </div>
          </div>
          <hr>
        </div>

        <div class="position">
          <p class="date text-primary">Hạn chót: 27/12/2020</p>
          <p class="title" title="Bấm để xem thêm thông tin"><a href="#" onclick="displayDescribe(3)">Senior Software Engineer (Java/Golang)</a></p>
          <p class="address">Đà Nẵng</p>
          <div class="describe d-none">
            <h3>Mô tả công việc</h3>
            <ul>
              <li>Tham gia vận hành và phát triển hệ thống ZingID và Game Payment</li>
              <li>Nghiên cứu và ứng dụng vào hệ thống nhằm đảm bảo độ tin cậy và khả năng mở rộng</li>
              <li>Phát triển các công vụ đáp ứng các công việc vận hành</li>
            </ul>
            <h3>Yêu cầu</h3>
            <ul>
              <li>Có kinh nghiệm Java Core từ 2 năm trở lên (open level tới Senior)</li>
              <li>Có kinh nghiệm CSDL RDBMS, NoSQL</li>
              <li>Có kinh nghiệm sử dụng caching</li>
              <li>Nắm vững cấu trúc dữ liệu và thuật toán cơ bản</li>
              <li>Hiểu về kiến trúc Microservices, HTTP protocol, REST/SOAP webservice</li>
              <li>Có kiến thức cơ bản với Linux</li>
              <li>Có kinh nghiệm sử dụng Messaging System: kaffa, ZeroMQ, Rabbit MQ là 1 lợi thế</li>
              <li>Tư duy tích cực, có kỹ năng giải quyết vấn đề, phối hợp tốt với các thành viên khác, nhiệt tình tham gia các hoạt động xây dựng team</li>
            </ul>
            <div class="text-center">
              <button type="submit" class="btn btn-primary btn-sm">Ứng tuyển</button>
            </div>
          </div>
          <hr>
        </div>

        <div class="position">
          <p class="date text-primary">Hạn chót: 27/12/2020</p>
          <p class="title" title="Bấm để xem thêm thông tin"><a href="#" onclick="displayDescribe(4)">Senior Data Engineer</a></p>
          <p class="address">Hà Nội</p>
          <div class="describe d-none">
            <h3>Mô tả công việc</h3>
            <ul>
              <li>Tham gia vận hành và phát triển hệ thống ZingID và Game Payment</li>
              <li>Nghiên cứu và ứng dụng vào hệ thống nhằm đảm bảo độ tin cậy và khả năng mở rộng</li>
              <li>Phát triển các công vụ đáp ứng các công việc vận hành</li>
            </ul>
            <h3>Yêu cầu</h3>
            <ul>
              <li>Có kinh nghiệm Java Core từ 2 năm trở lên (open level tới Senior)</li>
              <li>Có kinh nghiệm CSDL RDBMS, NoSQL</li>
              <li>Có kinh nghiệm sử dụng caching</li>
              <li>Nắm vững cấu trúc dữ liệu và thuật toán cơ bản</li>
              <li>Hiểu về kiến trúc Microservices, HTTP protocol, REST/SOAP webservice</li>
              <li>Có kiến thức cơ bản với Linux</li>
              <li>Có kinh nghiệm sử dụng Messaging System: kaffa, ZeroMQ, Rabbit MQ là 1 lợi thế</li>
              <li>Tư duy tích cực, có kỹ năng giải quyết vấn đề, phối hợp tốt với các thành viên khác, nhiệt tình tham gia các hoạt động xây dựng team</li>
            </ul>
            <div class="text-center">
              <button type="submit" class="btn btn-primary btn-sm">Ứng tuyển</button>
            </div>
          </div>
          <hr>
        </div>

        <div class="position">
          <p class="date text-primary">Hạn chót: 27/12/2020</p>
          <p class="title" title="Bấm để xem thêm thông tin"><a href="#" onclick="displayDescribe(5)">Senior QC Engineer (IoT)</a></p>
          <p class="address">Hà Nội</p>
          <div class="describe d-none">
            <h3>Mô tả công việc</h3>
            <ul>
              <li>Tham gia vận hành và phát triển hệ thống ZingID và Game Payment</li>
              <li>Nghiên cứu và ứng dụng vào hệ thống nhằm đảm bảo độ tin cậy và khả năng mở rộng</li>
              <li>Phát triển các công vụ đáp ứng các công việc vận hành</li>
            </ul>
            <h3>Yêu cầu</h3>
            <ul>
              <li>Có kinh nghiệm Java Core từ 2 năm trở lên (open level tới Senior)</li>
              <li>Có kinh nghiệm CSDL RDBMS, NoSQL</li>
              <li>Có kinh nghiệm sử dụng caching</li>
              <li>Nắm vững cấu trúc dữ liệu và thuật toán cơ bản</li>
              <li>Hiểu về kiến trúc Microservices, HTTP protocol, REST/SOAP webservice</li>
              <li>Có kiến thức cơ bản với Linux</li>
              <li>Có kinh nghiệm sử dụng Messaging System: kaffa, ZeroMQ, Rabbit MQ là 1 lợi thế</li>
              <li>Tư duy tích cực, có kỹ năng giải quyết vấn đề, phối hợp tốt với các thành viên khác, nhiệt tình tham gia các hoạt động xây dựng team</li>
            </ul>
            <div class="text-center">
              <button type="submit" class="btn btn-primary btn-sm">Ứng tuyển</button>
            </div>
          </div>
          <hr>
        </div>

        <ul class="pagination">
          <li class="page-item"><a class="page-link" href="javascript:void(0);"><i class="fas fa-backward"></i></a></li>
          <li class="page-item"><a class="page-link" href="javascript:void(0);">1</a></li>
          <li class="page-item active"><a class="page-link" href="javascript:void(0);">2</a></li>
          <li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
          <li class="page-item"><a class="page-link" href="javascript:void(0);">4</a></li>
          <li class="page-item"><a class="page-link" href="javascript:void(0);">5</a></li>
          <li class="page-item"><a class="page-link" href="javascript:void(0);">6</a></li>
          <li class="page-item"><a class="page-link" href="javascript:void(0);">...</a></li>
          <li class="page-item"><a class="page-link" href="javascript:void(0);"><i class="fas fa-forward"></i></a></li>
        </ul>
      </div>
      <div class="col-md-4">
        <section class="searchPosition">
          <h2>Tìm kiếm cơ hội nghề nghiệp</h2>
          <form>
            <div class="form-group">
              <input type="text" class="form-control" id="keyWord" onfocus="this.placeholder=''" onblur="this.placeholder='Từ khóa'" placeholder="Từ khóa">
            </div>
            <div class="form-group">
              <select class="form-control" id="selectPosition">
                <option selected disabled>Ngành nghề</option>
                <option>Kỹ thuật / Lập trình</option>
                <option>Kinh doanh / Tiếp thị</option>
                <option>Thiết kế / Sáng tạo</option>
                <option>Hỗ trợ kinh doanh</option>
              </select>
            </div>
            <div class="form-group">
              <select class="form-control">
                <option selected disabled>Nơi làm việc</option>
                <option>TP. Hồ Chí Minh</option>
                <option>Đà Nẵng</option>
                <option>Hà Nội</option>
              </select>
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-primary btn-sm">Tìm kiếm</button>
            </div>
          </form>
        </section>

        <section class="registerNotification">
          <h2>Đăng kí nhận tin qua email</h2>
          <form>
            <div class="form-group">
              <input type="text" class="form-control" id="fullName" onfocus="this.placeholder=''" onblur="this.placeholder='Họ và tên'" placeholder="Họ và tên">
            </div>
            <div class="form-group">
              <input type="email" class="form-control" id="email" onfocus="this.placeholder=''" onblur="this.placeholder='Email'" placeholder="Email">
            </div>
            <div class="text-center">
              <button type="submit" class="btn btn-primary btn-sm">Đăng kí</button>
            </div>
          </form>
        </section>

        <section class="advice">
          <h2>Lời khuyên dành cho ứng viên</h2>
          <p>Để gây ấn tượng tốt với nhà tuyển dụng, bạn cần chuẩn bị tốt hồ sơ của mình. Đừng gửi cho chúng tôi một bản sơ yếu lý lịch chung chung. Một hồ sơ tốt không chỉ được trình bày rõ ràng, mà còn phải được tổ chức tốt với những mục tiêu cụ thể, thể hiện đầy đủ kinh nghiệm bản thân, bằng cấp, kỹ năng… đặc biệt nên có những điểm nhấn cho các thông tin bạn muốn truyền tải tới nhà tuyển dụng. </p>
        </section>
      </div>
    </div>
  </div>
</div>

<?php include "footer.php"; ?>